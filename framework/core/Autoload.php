<?php
namespace liw;

class Autoload
{
    /**
     * Массив, где ключ - это префикс пространства имен,
     * а значение - это путь к этому пространству
     * 
     * Пока что строка
     * @var string
     */
    private $prefix = 'liw\\';
    private $path = '';

    public function register()
    {
        
        /**
         * массив, где ключ - это префикс пространства имен,
         * а значение - это путь к этому пространству
         * 
         * Перевел в этот метод что бы каждый раз не перезаписавала это свойство
         */
        $this->path = dirname(__DIR__) . DIRECTORY_SEPARATOR;
        
        spl_autoload_register([$this, 'MyLoader']);
    }
    
    private function MyLoader($loadClass)
    {
        $length = strlen($this->prefix);
        
        if (strncmp($this->prefix, $loadClass, $length) !== 0) {
 
            return;
        }
        
        $relative_class = substr($loadClass, $length);
        $file = str_replace('\\', '/', "{$this->path}$relative_class.php");
        
        if (file_exists($file)) {
            require $file;
        }
    }
}