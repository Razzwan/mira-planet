<?php
namespace liw\core;

use liw\core\base\validation\Clean;
use liw\core\base\Controller;
use liw\core\helper\ErrorHandler;

class Router
{
    /**
     * Преобразует url в массив
     */
    private function getFragmentsFromUrl()
    {
        /**
         * Разбиваем адресную строку на массив
         */
        $url = explode('/', trim(Clean::url($_SERVER['REQUEST_URI']), '/'));

        /**
         * Массив полученный из url
         * @var array
         */
        $fragments = [];

        /**
         * Если url непустой заполняем массив
         * переводим все полученное в нижний регистр избавляемся от опасных символов
         */
        foreach ($url as $key => $value) {
            if (!empty($value)) {
                $fragments[] = $value;
            }
        }

        return $fragments;
    }

    /**
     * Обрабатывает запрос и посылает ответ на исполнение
     * @param $config
     * @param $lang
     */
    public function start($config, $lang)
    {
        // Сохраняем конфиг в глобальной переменной
        Liw::$config = $config;

        // Сохраняем язык в глобальной переменной
        Liw::$lang = $lang[Liw::$config['def_lang']];

        // получаем массив из url
        $fragments = $this->getFragmentsFromUrl();

        // определяем контроллер
        $controller = array_shift($fragments) ?: Liw::$config['def_route'];

        // определяем действие (или, иначе, вид)
        $action = array_shift($fragments);

        // определяем атрибуты
        // в массиве $fragments как раз только они и остались

        $error = new ErrorHandler;

        // создаем экземпляр контроллера, если он существует и вызываем его метод (действие(вид)), передавая атрибуты
        try {
            $error->error();
            $this->invoke($controller, $action, $fragments);
        }
        catch (\Exception $e) {
            $this->catch_errors($e);
        }
    }
    
    /**
     * @param string $class
     * @param string $action
     * @param array $attributes
     * @throws \Exception
     */
    protected function invoke ($controller, $action, array $attributes) {
        $controller_route = '\liw\controllers\\' . ucfirst($controller) . 'Controller';

        if (!class_exists($controller_route)) {
            Controller::$now = Liw::$lang['no_controller'];

            throw new \Exception(Liw::$lang['no_controller'] . $controller);
        }

        // зачем? Нам нужна эта переменная для поиска папи с видами.
        // задаем в глобальной области видимости название
        // текущего контроллера (в нижнем регистре)
        Controller::$now = $controller;

        $instance = new $controller_route();
        
        $action = $action ?: $instance->default_action;
        
        if (!method_exists($instance, $action)) {
           throw new \Exception(Liw::$lang['no_action'] . $action);
        }

        /**
         * Если переданный класс-контроллер существует, то создаем его объект и
         * вызываем метод-действие(вид), передавая в него атрибуты
         *
         * Если переданого действия не существует, то будет вызвано действие по умолчаию,
         * заданное в контроллере, если в контроллере не задано действие по-умолчанию
         */
        call_user_func_array([$instance, $action], $attributes);
    }

    /**
     * Метод которой ловет ошибки
     * @param  object $e
     */
    protected function catch_errors($e)
    {
        $view = new \liw\core\base\View();
        if (!DEVELOP){
            $view->error([
                'msg' => Liw::$lang['error'],
            ]);
        } else {
            if  ($e->getFile() != PATH . 'core\helper\ErrorHandler.php') {
                $view->error([
                    // message ...
                    'msg' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine()
                ]);
            } else {
                $view->error([
                    'msg' => $e->getMessage()
                ]);
            }
        }
    }
}
