<?php
namespace liw\core\helper;

/**
 * Класс вспомогательных методов для разработки
 */
class Develop
{
    static public function print_arr($arr)
    {
        if (DEVELOP)
            echo '<pre>' . print_r($arr, 1) . '</pre>';

    }
}
