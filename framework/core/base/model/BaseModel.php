<?php
namespace liw\core\base\model;

use liw\core\base\validation\Clear;

class BaseModel {
    /**
     * Ассоциативный одномерный массив данных
     * @var array
     */
    public $data = [];

    /**
     * Возвращает данные, пропущенные через фильтры
     * и сохраняет их в ассоциативном массиве экземпляра класса
     * @param $data
     * @return bool|string
     */
    public  function getData($data = [])
    {
        return $this->data = array_map(function ($value) {
            return Clear::user_data($value);
        }, $data);
    }
}
