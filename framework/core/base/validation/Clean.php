<?php
namespace liw\core\base\validation;

/**
 * Статичный класс используется для проверки пользовательского ввода
 * и подготовки переменных перед выводом
 *
 * Class Clean
 * @package liw\core\base\validation
 */
class Clean {
    /**
     * Очищает от нежелательных символов input
     *
     * @param $str
     * @return string
     */
    static public function input($str)
    {
        return $str = filter_var($str, FILTER_SANITIZE_STRING);
    }

    /**
     * Очищает от запрещенных символов url и переводит в нижний регистр
     *
     * @param $url
     * @return string
     */
    static public function url($url)
    {
        return $url = strtolower(filter_var($url, FILTER_SANITIZE_URL));
    }
}
