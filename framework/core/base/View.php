<?php
namespace liw\core\base;

use liw\core\base\Controller;
use liw\core\Liw;

/**
 * Класс отображения
 * для доступа информации в видах используются следующие переменные:
 * 1. $this->model - данные передаваемые из модели в вид
 * 2. $this->title - текущее название страницы может быть задано в виде
 */
class View
{
    /**
     * html- разметка текущего вида страницы
     * @var string
     */
    public $view = '';
    public $view_path = '';

    /**
     * Массив данных, передаваемых в вид из Модели
     * @var array
     */
    public $model = [];

    /**
     * Массив сообщений об ошибках
     * @var array
     */
    public $msg = [];

    /**
     * @var string
     * путь к папке, где лежат виды
     */
    private $path_views = '';

    public $title = '';

    /**
     * Устанавливает используемый контроллер
     * и в соответствии с ним присваивает название папки
     * @param $Controller
     */
    public function __construct()
    {
        $this->title = Liw::$config['site_name'];
        $this->path_views = Controller::$now;
    }

    /**
     * Загружает переданный вид в документ
     * и туда же подгружает лэйаут
     * @param $view
     * @param array $data
     */
    public function show($view, $model = null)
    {
        $this->view_path = PATH . 'views/' . $this->path_views . '/' . $view . '.php';
        ob_start();
        include $this->view_path;
        $this->view =  ob_get_contents();
        ob_end_clean();
        require_once PATH . 'views/layouts/' . Liw::$config['def_layout'] . '.php'; //подключение layout
        exit();
    }

    public function error($msg)
    {
        if(ob_get_length()) ob_end_clean(); // проверяем пустой ли буфер, и если он непустой, то очищаем его
        $this->msg = $msg;
        $this->view = PATH . 'views/error.php';
        ob_start();
        include $this->view;
        $this->view = ob_get_contents();
        ob_end_clean();
        require_once PATH . 'views/layouts/' . Liw::$config['def_layout'] . '.php';
        exit();
    }

    public function msg()
    {
        echo 'message from View!!!';
        exit();
    }

}
