<?php
/**
 * Фаил конфигурации для сайта
 * @return  array Массив настроек
 *
 * ключи ассоциативного массива:
 * 1. Только нижний регистр
 * 2. Разделение слов: символом нижнего подчеркивания "_"
 * пример: def_path - верно; defPath,defpath,def-path - неверно
 */

$config = [
    'site_name' => 'MIRA', //Название сайта
    'charset'   => 'UTF-8',//Кодировка
    'def_route' => 'main', //Контроллер по умолчанию (в нижнем регистре)5555
    'def_layout'=> 'layout', //Вид по умолчанию
    'def_lang'  => 'ru',   //Язык
    
    // Подключение к базе данных
    'db' => [
        'host' => 'localhost',
        'user' => 'root',
        'pass' => '',
        'name' => '',
    ],
];

return $config;
