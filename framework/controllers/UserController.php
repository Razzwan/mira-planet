<?php
namespace liw\controllers;

use liw\core\base\Controller;

class UserController extends Controller
{


    public function index()
    {
        $this->view->show('index');
    }

    public function registration()
    {
        $this->model('user');
        $res = $this->user->index();
        $res['title'] = 'Название страницы';

        if (!empty($_POST['login'])) {
            foreach ($_POST as $key=>$value){
                if (!empty($_POST[$key])){
                    $res[$key] = $value;
                }
            }
            $this->view->show('registration', $res);
        } else {
            $this->view->show('registration', $res);
        }
    }

    public function login()
    {   
        $this->view->show('index');
    }
}
