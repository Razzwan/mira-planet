<?php
/**
 * Во всех видах доступны переменные класса View через конструкцию $this->variable
 */

$this->title = $model['title'];
?>

<div class = "reg">
    <form action="/user/registration" method="post">
        <table>
            <caption>Регистрация:</caption>
            <tr>
                <td><input type="text" id="login" name = "login" placeholder="login" value="<?=$model['login']?>" autofocus/></td>
            </tr>
            <tr>
                <td><input type="password" id="password" name = "password" placeholder="password"/></td>
            </tr>
            <tr>
                <td><input type="password" id="password2" name = "password2" placeholder="повторите password" /></td>
            </tr>
            <tr>
                <td><input type="submit" name="reg" value="Зарегистрироваться" /></td>
            </tr>
        </table>
    </form>
</div>
