$(document).ready(function($){
    /**
     *  Левое меню наведение, увод
     */
    $("#left_menu input[type='submit']").hover(
    	function(){    		
    		$(this).css({
    			"opacity" : "1"
    		});
    	},
    	function(){
    		$(this).css({
    			"opacity" : "0.7"
    		});
    	}
    );

    /**
     * добавляет рамку активному элементу
     */
    $("input").focus(function(){
    $(this)
        .removeClass("error")
        .removeClass("prov")
        .addClass("active");
    }).blur(function(){
    	$(this)
            .removeClass("active");
    });

    /**
     * Функция добавляет визуальное подтверждение проверки рег выражением
     *
     * @param obj - объект, который тестируется на совпадение с регВ
     * @param regV - Регулярное выражение
     */
    var
        login = $("#login"),
        password = $("#password"),
        password2 = $("#password2"),
        fRegV = function(obj,regV){
            var str = $.trim(obj.val());
            if(str==''){
                obj
                    .removeClass("prov")
                    .removeClass("error_input");
            }else{
                if (str.search(regV)==-1){
                    obj
                        .removeClass("prov")
                        .addClass("error_input")
                        .val(str);
                } else {
                    obj
                        .removeClass("error_input")
                        .addClass("prov")
                        .val(str);
                }
            }
        },
        //любые русские или английские буквы, подчеркивание или дефиз
        regV_login = /^[a-zA-Z0-9а-яА-Я_\-]{2,}$/,
        //любые непробельные символы
        regV_email = /^[\s\S]+[@]{1}[\s\S]+[\.]{1}[\s\S]+$/,
        //любые символы минимум 6 раз
        regV_pass = /^.{6,}$/,
        regV_reg_code = /^[0-9]{3}$/
    ;

    /**
     * Проверка влидации при уведении курсора
     */
    login.blur(function(){
        fRegV($(this),regV_login);
    });

    password.blur(function(){
        fRegV($(this),regV_pass);
    });

    password2.blur(function(){
        if($(this).val()==password.val() && $(this).val()!=''){
            $(this)
                .addClass("prov")
        } else {
            $(this)
                .addClass("error_input");
            if($(this).val()==password.val() && $(this).val()==''){
                $(this)
                    .removeClass("error_input")
                    .removeClass("prov");
            }
        }
    });
}); //конец  ready